FROM node:14

#Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

#install dependecies..
RUN npm install

# copy app source code..
COPY . .

# expose port
EXPOSE 3000

#Build the app
RUN npm run build

#Copy certificates into dist folder
COPY certificates ./dist/certificates

#specify start up command
CMD [ "STAGE=dev node", "dist/main.js" ]