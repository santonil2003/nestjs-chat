import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/decorator/get-user.decorator';
import { Roles } from 'src/auth/decorator/roles.decorator';
import { User } from 'src/auth/entity/user.entity';
import { Role } from 'src/auth/enums/role.enum';
import { RolesGuard } from 'src/auth/guard/roles.guard';
import { CreateAccountDto } from './dto/create-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { AccountService } from './service/account/account.service';

@Controller('account')
@UseGuards(AuthGuard())
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Post()
  @UseGuards(RolesGuard)
  @Roles(Role.SUPER_ADMIN)
  create(@Body() createAccountDto: CreateAccountDto) {
    return this.accountService.create(createAccountDto);
  }

  @Get()
  @UseGuards(RolesGuard)
  @Roles(Role.SUPER_ADMIN)
  findAll() {
    return this.accountService.findAll();
  }

  @Get(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.ADMIN, Role.SUPER_ADMIN)
  async findOne(@Param('id') id: string, @GetUser() currentUser: User) {
    return await this.accountService.findOne(id, currentUser);
  }

  @Patch(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.ADMIN, Role.SUPER_ADMIN)
  update(
    @Param('id') id: string,
    @Body() updateAccountDto: UpdateAccountDto,
    @GetUser() currentUser: User,
  ) {
    return this.accountService.update(id, updateAccountDto, currentUser);
  }

  @Delete(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.SUPER_ADMIN)
  async remove(@Param('id') id: string, @GetUser() currentUser: User) {
    return await this.accountService.remove(id, currentUser);
  }

  @Get('/restore/:id')
  @UseGuards(RolesGuard)
  @Roles(Role.SUPER_ADMIN)
  async restore(@Param('id') id: string, @GetUser() currentUser: User) {
    return await this.accountService.restore(id, currentUser);
  }

  @Get('/find-account/:id/:appKey')
  async findAccount(@Param('id') id: string, @Param('appKey') appKey: string) {
    return await this.accountService.findOneByIdAndAppKey(id, appKey);
  }
}
