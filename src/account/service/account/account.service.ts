import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateAccountDto } from 'src/account/dto/create-account.dto';
import { UpdateAccountDto } from 'src/account/dto/update-account.dto';
import { Account } from 'src/account/entity/account.entity';
import { User } from 'src/auth/entity/user.entity';
import { Role } from 'src/auth/enums/role.enum';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
  ) {}

  async create(createAccountDto: CreateAccountDto) {
    try {
      const { account_id, account_name } = createAccountDto;

      const model = this.accountRepository.create({
        account_id,
        account_name,
      });

      await this.accountRepository.save(model);

      return model;
    } catch (error) {
      switch (error.errno) {
        case 1062:
          throw new ConflictException(`${error.message}`);
        default:
          throw new InternalServerErrorException(error.message);
      }
    }
  }

  async findAll() {
    const query = this.accountRepository.createQueryBuilder('account');

    query.orderBy('account.account_name', 'ASC');

    const accounts = await query.getMany();

    return accounts;
  }

  /**
   *
   * @param id findOne
   * @param user
   * @returns
   */
  async findOne(id: string, user: User): Promise<Account> {
    try {
      if ([Role.SUPER_ADMIN].includes(user.role) || user.account.id === id) {
        const account =  await this.accountRepository.findOne({ id });

        if(account){
          return account;
        }

      }

      throw new NotFoundException(`Account not found.`);

    } catch (error) {
      throw new NotFoundException(`${error.message}`);
    }
  }

  /**
   * find account by id and appkey for sign up purpose..
   * @param id 
   * @param appKey 
   * @returns 
   */
  async findOneByIdAndAppKey(id: string, appKey: string): Promise<Account> {
    try {

      return await this.accountRepository.findOneOrFail({ id: id, app_key: appKey });

    } catch (error) {
      throw new NotFoundException(`Invalid account.`);
    }
  }



  async update(
    id: string,
    updateAccountDto: UpdateAccountDto,
    user: User,
  ): Promise<Account> {
    const model: Account = await this.findOne(id, user);

    const { account_name, re_generate_app_key } = updateAccountDto;

    model.account_name = account_name;

    if (re_generate_app_key) {
      model.app_key = uuidv4();
    }

    await this.accountRepository.save(model);

    return model;
  }

  async remove(id: string, user: User): Promise<Account> {
    const model = await this.findOne(id, user);
    await this.accountRepository.softRemove(model);
    return model;
  }

  async restore(id: string, user: User): Promise<Account> {
    await this.accountRepository.restore(id);
    const model = await this.findOne(id, user);
    return model;
  }
}
