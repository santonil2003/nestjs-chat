import { IsNotEmpty } from 'class-validator';


export class CreateAccountDto {

  @IsNotEmpty()
  account_id: number;

  @IsNotEmpty()
  account_name:string
}