import { Optional } from '@nestjs/common';
import { PartialType } from '@nestjs/mapped-types';
import { CreateAccountDto } from './create-account.dto';

export class UpdateAccountDto extends PartialType(CreateAccountDto) {
    @Optional()
    re_generate_app_key: boolean
}
