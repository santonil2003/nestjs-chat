import { User } from "src/auth/entity/user.entity";
import { ChatGroup } from "src/chat/entity/chat-group.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, Generated, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";


@Entity({name: 'tigga_chat_account'})
export class Account{

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  account_id: number

  @Column()
  account_name:string

  @Column({ unique: true })
  @Generated("uuid")
  app_key:string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;


  @OneToMany((type) => User, user => user.account)
  users: User[];

  @OneToMany((type) => ChatGroup, group => group.account)
  groups: ChatGroup[];

}