import { forwardRef, Module } from '@nestjs/common';
import { AccountService } from './service/account/account.service';
import { AccountController } from './account.controller';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './entity/account.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Account
    ]),
    forwardRef(() => AuthModule)
  ],
  providers: [AccountService],
  controllers: [AccountController],
  exports:[AccountService]
})
export class AccountModule {}
