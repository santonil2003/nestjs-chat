import { Process, Processor } from "@nestjs/bull";
import { Job } from "bull";

@Processor('file-queue')
export class FileProcessor{

    @Process('unlink-file-job')
    unlinkFile(job: Job<unknown>){
        console.log('Unlink File JOB');
        console.log(job.id);
        console.log(job.data);
    }
}