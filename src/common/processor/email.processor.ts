import { Process, Processor } from "@nestjs/bull";
import { Job } from "bull";

@Processor('email-queue')
export class EmailProcessor{

    @Process('send-email-job')
    sendEmail(job: Job<unknown>){
        console.log(job.id);
        console.log(job.data);
    }
}