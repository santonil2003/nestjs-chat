import { Injectable } from '@nestjs/common';
import { Observable, of } from 'rxjs';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  /**
   * http service error handler
   * @param operation
   * @param result
   * @returns
   */
  handleHttpServiceError<T>(operation = 'operation', result?: T) {
    return (errorResponse: any): Observable<T> => {
      // log it using winston...
      console.log(`${operation}`, errorResponse.response.data);

      // Let the app keep running by returning an empty result.
      if (result !== undefined) {
        return of(result as T);
      }

      return of(errorResponse.response.data as T);
    };
  }
}
