import { InjectQueue } from "@nestjs/bull";
import { Injectable } from "@nestjs/common";
import { Queue } from "bull";

@Injectable()
export class EmailProducerService {
    constructor(@InjectQueue('email-queue') private queue: Queue){

    }

    async sendEmail(message:string){
        await this.queue.add('send-email-job', {
            body:message
        }, {
            delay: 10000
        });
    }
}