import { Logger, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './common/service/app.service';
import { configValidationSchema } from './common/schema/config.schema';
import { AuthModule } from './auth/auth.module';
import { FilesModule } from './files/files.module';
import { FileSubscriber } from './files/subscriber/file.subscriber';
import { BullModule } from '@nestjs/bull';
import { EmailProducerService } from './common/service/email.producer.service';
import { EmailProcessor } from './common/processor/email.processor';
import { FileProcessor } from './common/processor/file.processor';
import { ChatModule } from './chat/chat.module';
import { AccountModule } from './account/account.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.env.stage.${process.env.STAGE}`],
      validationSchema: configValidationSchema,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          type: 'mysql',
          host: configService.get('DB_HOST'),
          port: configService.get('DB_PORT'),
          username: configService.get('DB_USERNAME'),
          password: configService.get('DB_PASSWORD'),
          database: configService.get('DB_DATABASE'),
          timezone: '+10:00',
          subscribers: [FileSubscriber],
          synchronize: false,
          autoLoadEntities: true,
          entities: ["dist/**/*.entity{.ts,.js}"],
          extra: {
            connectionLimit: 20,
          },
        };
      },
    }),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          redis: {
            host: configService.get('REDIS_HOST'),
            port: configService.get('REDIS_PORT'),
          },
        };
      },
    }),
    BullModule.registerQueue(
      {
        name: 'email-queue',
      },
      {
        name: 'file-queue',
      },
    ),
    AccountModule,
    AuthModule,
    FilesModule,
    ChatModule,
  ],
  controllers: [AppController],
  providers: [
    Logger,
    AppService,
    EmailProducerService,
    EmailProcessor,
    FileProcessor,
  ],
})
export class AppModule {}
