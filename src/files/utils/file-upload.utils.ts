import { ForbiddenException} from '@nestjs/common';
import { extname } from 'path';

/**
 * valdi file filter
 * @param req 
 * @param file 
 * @param callback 
 * @returns 
 */
export const validFileFilter = (req, file, callback) => {



    const validMimeTypes = [
        'text/plain',
        'ext/richtext',
        'text/x-vcard',
        'x-world/x-vrml',
        'application/x-abiword',
        'application/x-freearc',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.oasis.opendocument.presentation',
        'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/vnd.oasis.opendocument.spreadsheet',
        'application/vnd.oasis.opendocument.text',
        'application/pdf',
        'image/gif',
        'image/bmp',
        'image/jpeg',
        'image/pipeg',
        'image/svg+xml',
        'image/png',
        'image/tiff',
        'audio/mpeg',
        'audio/ogg',
        'audio/wav',
        'audio/webm',
        'video/webm',
        'video/ogg',
        'video/mp4',
        'video/quicktime'
    ];

    if(!validMimeTypes.includes(file.mimetype)){
        return callback(
            new ForbiddenException('Invalid mime type.'),
            false,
          );
    }
    

    /**
     * add file attributes to body params so that dto can validate it..
     */
    req.body.originalname = file.originalname;
    req.body.mimetype = file.mimetype;

    callback(null, true);

};

/**
 * allow Image only
 * @param req
 * @param file
 * @param callback
 * @returns
 */
export const imageFileFilter = (req, file, callback) => {

  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(
      new ForbiddenException('Only image files are allowed!'),
      false,
    );
  }
  callback(null, true);
};

/**
 *
 * @param req generate upload file name
 * @param file
 * @param callback
 */
export const generateUniqueFileName = (req, file, callback) => {
  const name = file.originalname.split('.')[0];
  const fileExtName = extname(file.originalname);
  const randomName = Array(4)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${name}-${randomName}${fileExtName}`);
};
