import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';


@Entity({name: 'tigga_chat_file'})
export class File {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 255 })
  name: string;

  @Column('varchar', { length: 255 })
  originalname: string;

  @Column('varchar', { length: 255 })
  filename: string;

  @Column()
  mimetype: string;

  @Column('varchar', { length: 255 })
  path: string;

  @Column({type: "float"})
  size: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @BeforeInsert()
  updateName(){
    this.name = this.name+' haha';
  }
}
