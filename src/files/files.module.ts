import { Module } from '@nestjs/common';
import { FilesService } from './service/files.service';
import { FilesController } from './files.controller';
import { FilesRepository } from './repository/files.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      FilesRepository
    ]),
    MulterModule.registerAsync({
      imports : [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService:ConfigService) => {
        return {
          dest: configService.get('UPLOAD_BASE_PATH')
        }
      }
    }),
    AuthModule
  ],
  controllers: [FilesController],
  providers: [FilesService]
})
export class FilesModule {}
