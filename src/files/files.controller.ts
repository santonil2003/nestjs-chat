import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  BadRequestException,
  InternalServerErrorException,
  Res,
  NotFoundException,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';
import { FilesService } from './service/files.service';
import { CreateFileDto} from './dto/create-file.dto';
import { UpdateFileDto } from './dto/update-file.dto';
import { generateUniqueFileName, validFileFilter } from './utils/file-upload.utils';
import { diskStorage } from 'multer';
import { validate } from 'class-validator';
import { UploadFileDto } from './dto/upload-file.dto';
import { getConnection, getManager } from 'typeorm';
import { AuthService } from 'src/auth/service/auth.service';
import { User } from "src/auth/entity/user.entity";


@Controller('files')
export class FilesController {
  constructor(private readonly filesService: FilesService, private authService: AuthService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file',{
    storage: diskStorage({
      destination: './uploads/files',
      filename: generateUniqueFileName,
    }),
    fileFilter:validFileFilter,
  }))
  async create(
    @UploadedFile() file: Express.Multer.File,
    @Body() uploadFileDto: UploadFileDto
  ) {

    /**
     * check if file is uploaded....
     */
    if(!file.hasOwnProperty('path')){
      throw new InternalServerErrorException("File upload failed.");
    }


    /**
     * sample example showing ad hoc validation and data merging from different dto..
     */
    const newFile = new CreateFileDto();

    newFile.name = uploadFileDto.name;

    newFile.originalname = file.originalname;
    newFile.filename = file.filename;
    newFile.mimetype = file.mimetype;
    newFile.path = file.path;
    newFile.size = file.size;

    /**
     * ad hoc validation...
     */
    const errors = await validate(newFile);

    if(Array.isArray(errors) && errors.length>0){
      let validationErrors = [];

      errors.map(error=>{
        for(let property in error.constraints){
          validationErrors.push(error.constraints[property]);
        }
      });

      throw new BadRequestException(validationErrors);
    }

    

    return this.filesService.create(newFile);
  }

  @Get('/get/:id')
  async getFile(@Param('id') id:string, @Res() res){
    const file =  await this.filesService.findOne(id);

    if(!file){
      throw new NotFoundException(`File with id ${id} does not exist.`);
    }

    return res.download(file.path);
  }


  /**
   * to run raw query without entity use getManager() from typeorm
   * @returns 
   */
  @Get('/get-all')
  async queryAll(){
    // return await getManager().query(`select * from task where title like ?`, ['%user%']);
    // return await getManager().createQueryBuilder().select('task.*').from('task','task').where('task.title like :title', {title:'%1%'}).getRawMany();


    /*
 // insert with query builder..
      const user = await this.authService.getUser('2005a333-3a3e-4c77-a0fb-eda3e90875b5');
       const insertTest = getManager().createQueryBuilder().insert().into('task').values([
         {title:"title", description: 'desc', status: 'OPEN', user: user}
       ]).execute();

       return insertTest;
       */

// update with query builder..

/*
       const user = await this.authService.getUser('2005a333-3a3e-4c77-a0fb-eda3e90875b5');
       return await getManager().createQueryBuilder().update('task').set({
         title: "title 1",
        user: user,
       }).where("id=:id",{id:'b5aa30ad-8b13-4557-8b1a-269833a3da3b'}).execute();
       */

       /**
        * 
       // insert without entity...
       const insertTest = getManager().createQueryBuilder().insert().into('log').values([
         {level:1, category: 'mycat', log_time: 12, prefix: 'prefix 1', message: 'hi'}
       ]).execute();

              return insertTest;
       */



  }

  @Get()
  findAll() {
    return this.filesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.filesService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFileDto: UpdateFileDto) {
    return this.filesService.update(+id, updateFileDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.filesService.remove(+id);
  }
}
