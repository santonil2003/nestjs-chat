import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateFileDto } from '../dto/create-file.dto';
import { UpdateFileDto } from '../dto/update-file.dto';
import { File } from '../entity/file.entity';
import { FilesRepository } from '../repository/files.repository';

@Injectable()
export class FilesService {

  constructor(
    @InjectRepository(FilesRepository) private filesRepository: FilesRepository,
  ) {}

  async create(createFileDto: CreateFileDto): Promise<File> {
    return this.filesRepository.createFile(createFileDto);
  }

  findAll() {
    return `This action returns all files`;
  }

  async findOne(id: string) {

    const found = await this.filesRepository.findOne({id});

    if (!found) {
      throw new NotFoundException(`File with id ${id} does not exist.`);
    }

    return found;
  }

  update(id: number, updateFileDto: UpdateFileDto) {
    return `This action updates a #${id} file`;
  }

  remove(id: number) {
    return `This action removes a #${id} file`;
  }
}
