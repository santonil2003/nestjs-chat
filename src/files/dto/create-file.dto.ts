import { IsEmail, IsNotEmpty } from "class-validator";
import { PartialType } from '@nestjs/mapped-types';
import { UploadFileDto } from "./upload-file.dto";



export class CreateFileDto  extends PartialType(UploadFileDto){

  @IsNotEmpty()
  originalname: string;

  @IsNotEmpty()
  filename: string;

  @IsNotEmpty()
  mimetype: string;

  @IsNotEmpty()
  path: string;

  @IsNotEmpty()
  size: number;

}