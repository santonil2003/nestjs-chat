import { EntityRepository, Repository } from 'typeorm';
import { CreateFileDto } from '../dto/create-file.dto';
import { File } from '../entity/file.entity';

@EntityRepository(File)
export class FilesRepository extends Repository<File> {
  /**
   * create new file
   * @param createFileDto
   * @returns
   */
  async createFile(createFileDto: CreateFileDto): Promise<File> {
    const { name, originalname, filename, mimetype, path, size } =
      createFileDto;

    const data = {
        name, 
        originalname, 
        filename, 
        mimetype, 
        path, 
        size 
    };

    const newFile = this.create(data);

    await this.save(newFile);

    return newFile;
  }
}
