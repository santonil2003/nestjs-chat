import { EntitySubscriberInterface, EventSubscriber, InsertEvent } from "typeorm";
import { File } from "../entity/file.entity";

@EventSubscriber()
export class FileSubscriber implements EntitySubscriberInterface<File> {
    listenTo(){
        return File;
    }


    beforeInsert(event: InsertEvent<File>){
        event.entity.name = 'haha subscriber hu mai';

        console.log('subscriber',event.entity)
    }
}