import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as fs from 'fs';
import {
  utilities as nestWinstonModuleUtilities,
  WinstonModule,
} from 'nest-winston';
import { AppModule } from './app.module';
import * as winston from 'winston';
import { TransformInterceptor } from './common/interceptor/transform.interceptor';
import { RedisIoAdapter } from './chat/adaptor/redis.io.adapter';

async function bootstrap() {


  /**
   * SSL certificates configurations
   */
  const httpsOptions = {
    key: fs.readFileSync('./certificates/tiggadesk/tiggadesk.com.key'),
    cert: fs.readFileSync('./certificates/tiggadesk/STAR_tiggadesk_com.crt'),
    ca: [fs.readFileSync('./certificates/tiggadesk/My_CA_Bundle.ca-bundle')],
  };

  /**
   * init app with 3rd party logger called winstone
   */
  const app = await NestFactory.create(AppModule, {
    httpsOptions: httpsOptions,
    logger: WinstonModule.createLogger({
      format: winston.format.combine(
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winston.format.json(),
      ),
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),
        new winston.transports.File({
          level: 'error',
          filename: 'logs/error.log',
        }),
      ],
    }),
  });

  // enable validaton pipe globally
  app.useGlobalPipes(new ValidationPipe());

  // use globle intercetor..
  app.useGlobalInterceptors(new TransformInterceptor());

  // use custom web socket adaptor
  app.useWebSocketAdapter(new RedisIoAdapter(app));

  await app.listen(3000);
}
bootstrap();
