import {
    Controller,
    Get,
    Post,
    Body,
    Patch,
    Param,
    Delete,
    UseGuards,
  } from '@nestjs/common';
  import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/decorator/get-user.decorator';
import { Roles } from 'src/auth/decorator/roles.decorator';
import { User } from 'src/auth/entity/user.entity';
import { Role } from 'src/auth/enums/role.enum';
import { RolesGuard } from 'src/auth/guard/roles.guard';
import { CreateChatGroupDto } from '../dto/create-chat-group-dto';
import { ChatGroupService } from '../service/chat-group.service';

  @Controller('group')
  @UseGuards(AuthGuard())
  export class ChatGroupController {
    constructor(private readonly chatGroupService: ChatGroupService) {}

    @Post()
    @UseGuards(RolesGuard)
    @Roles(Role.SUPER_ADMIN, Role.ADMIN)
    create(@Body() createChatGroupDto: CreateChatGroupDto) {
      return this.chatGroupService.create(createChatGroupDto);
    }

    @Post('create-type-guest')
    createTypeGuest(@Body() createChatGroupDto: CreateChatGroupDto, @GetUser() currentUser: User) {

      createChatGroupDto.accountId = currentUser.accountId;
      createChatGroupDto.groupOwnerId = currentUser.id;
      
      return this.chatGroupService.create(createChatGroupDto);
    }
  
  
  }
  