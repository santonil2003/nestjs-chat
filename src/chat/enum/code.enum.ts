export enum ErroCode{
    INVALID_TOKEN = 401,
    FAILED_TO_ASSIGN_TICKET = 1001
}

export enum StatusCode{
    UNAUTHORIZED = 401,
    INTERNAL_SERVER_ERROR = 500
}