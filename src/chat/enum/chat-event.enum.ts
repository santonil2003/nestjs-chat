/**
 * Naming convention used in naming server side event
 * Unicast(_u_) -> to one user
 * Multicast(_m_) -> to one group
 * Broad cast(_b_) -> to all
 * Pull request (_p_)
 */
export enum ChatServerEvent {
  _u_error = '_u_error',
  _u_after_handle_connection = '_u_after_handle_connection',
  _u_private_message_received = '_u_private_message_received',
  _m_group_message_received = '_m_group_message_received',
  _m_p_group_created = '_m_p_group_created',
  _m_p_group_deleted = '_m_p_group_deleted',
  _m_agent_online = '_m_agent_online',
  _m_agent_offline = '_m_agent_offline',
  _m_p_guest_online = '_m_p_guest_online',
  _m_p_guest_offline= '_m_p_guest_offline',
  _u_guest_assigned_agent = '_u_guest_assigned_agent',
  _u_agent_assigned_guest = '_u_agent_assigned_guest',
  _u_agent_ended_chat = '_u_agent_ended_chat',
  _u_guest_ended_chat = '_u_guest_ended_chat'
}

export enum ChatClientEvent {
  load_chat_ui = 'load_chat_ui',
  load_chat_widget_ui= 'load_chat_widget_ui',
  fetch_chat_groups = 'fetch_chat_groups',
  fetch_agents = 'fetch_agents',
  fetch_assigned_guests = 'fetch_assigned_guests',
  view_private_conversation = 'view_private_conversation',
  view_group_conversation = 'view_group_conversation',
  send_private_message = 'send_private_message',
  send_group_message = 'send_group_message',
  guest_request_agent ='guest_request_agent',
  guest_ended_chat = 'guest_ended_chat',
  agent_ended_chat = 'agent_ended_chat'
}
