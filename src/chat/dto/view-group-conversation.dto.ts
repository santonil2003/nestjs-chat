import { IsNotEmpty, IsOptional } from "class-validator";

export class ViewGroupConversationDto {
  @IsNotEmpty()
  groupId:string;

  @IsOptional()
  page: number = 1;

  @IsOptional()
  limit: number = 20;
}