import { IsNotEmpty, IsOptional } from "class-validator";

export class SendPrivateMessageDto {
  @IsNotEmpty()
  sentToId:string;

  @IsNotEmpty()
  content:string;

  @IsNotEmpty()
  sentById:string;

  @IsOptional()
  ticket_id:string;
}