import { IsNotEmpty, IsOptional} from "class-validator";

export class CreateChatGroupDto {

  @IsNotEmpty()
  name:string;

  @IsOptional()
  display_name: string;

  @IsOptional()
  description: string;

  @IsNotEmpty()
  groupOwnerId: string;

  @IsOptional()
  params: string;

  @IsNotEmpty()
  accountId: string;

}