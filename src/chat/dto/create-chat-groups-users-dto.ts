import { IsNotEmpty, IsOptional} from "class-validator";

export class CreateChatGroupsUsersDto {

  @IsOptional()
  params: string;

  @IsNotEmpty()
  chatGroupId: string;

  @IsNotEmpty()
  userId: string;
}