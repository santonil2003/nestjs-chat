import { IsNotEmpty } from "class-validator";
import { User } from "src/auth/entity/user.entity";

export class CreateConnectedUserSocketDto {
  @IsNotEmpty()
  socket_id: string;

  @IsNotEmpty()
  user:User;
}