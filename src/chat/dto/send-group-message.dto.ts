import { IsNotEmpty } from "class-validator";

export class SendGroupMessageDto {
  @IsNotEmpty()
  sentToGroupId:string;

  @IsNotEmpty()
  content:string;

  @IsNotEmpty()
  sentById:string;
}