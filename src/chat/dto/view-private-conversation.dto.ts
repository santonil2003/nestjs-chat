import { IsNotEmpty, IsOptional } from "class-validator";

export class ViewPrivateConversationDto {
  @IsNotEmpty()
  patnerUserId:string;

  @IsOptional()
  page: number = 1;

  @IsOptional()
  limit: number = 20;
}