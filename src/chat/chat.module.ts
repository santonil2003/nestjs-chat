import { Logger, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountModule } from 'src/account/account.module';
import { AuthModule } from 'src/auth/auth.module';
import { ChatMessageAttachment } from './entity/chat-message-attachment.entity';
import { ChatMessage } from './entity/chat-message.entity';
import { ChatPrivateMessage } from './entity/chat-private-message.entity';
import { ChatGroupMessage } from './entity/chat-group-message.entity';
import { ChatGroup } from './entity/chat-group.entity';
import { ConnectedUserSocket } from './entity/connected-user-socket.entity';
import { SeenChatMessage } from './entity/seen-chat-message.entity';
import { ChatGateway } from './gateway/chat.gateway';
import { ChatMessageService } from './service/chat-message.service';
import { ChatPivateMessageService } from './service/chat-private-message.service';
import { ChatGroupMessageService } from './service/chat-group-message.service';
import { ChatGroupService } from './service/chat-group.service';
import { ConnectedUserSocketService } from './service/connected-user-socket.service';
import { ChatGroupController } from './controller/chat-group.controller';
import { ChatGroupsUsersService } from './service/chat-groups-users.service';
import { ChatGroupsUsers } from './entity/chat-groups-users.entity';
import { ChatGroupSubscriber } from './subscriber/chat-group.subscriber';
import { HttpModule } from '@nestjs/axios';
import { ChatService } from './service/chat.service';
import { ConfigService } from '@nestjs/config';
import { AppService } from 'src/common/service/app.service';


@Module({
    imports: [
      HttpModule.registerAsync({
        useFactory: () => ({
          timeout: 5000,
          maxRedirects: 5,
        }),
      }),
      AuthModule,
      AccountModule,
      TypeOrmModule.forFeature([
        ChatGroup,
        ChatGroupsUsers,
        ChatMessage,
        ChatGroupMessage,
        ChatPrivateMessage,
        ChatMessageAttachment,
        SeenChatMessage,
        ConnectedUserSocket
      ]),
    ],
    providers: [
      ConfigService,
      AppService,
      ChatGateway,
      Logger,
      ConnectedUserSocketService,
      ChatService,
      ChatGroupService,
      ChatGroupsUsersService,
      ChatMessageService,
      ChatPivateMessageService,
      ChatGroupMessageService,
      ChatGroupSubscriber
    ],
    controllers: [ChatGroupController],
  })


export class ChatModule {}
