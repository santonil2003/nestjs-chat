import {
  Logger,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsException,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { AuthService } from 'src/auth/service/auth.service';
import { User } from 'src/auth/entity/user.entity';
import { CreateConnectedUserSocketDto } from '../dto/create-connected-user-socket.dto';
import { ConnectedUserSocketService } from '../service/connected-user-socket.service';
import { ChatErrorInterface } from '../interface/chat-error.interface';
import { StatusCode, ErroCode } from '../enum/code.enum';
import { ChatServerEvent, ChatClientEvent } from '../enum/chat-event.enum';
import { UserService } from 'src/auth/service/user.service';
import { ChatGroupService } from '../service/chat-group.service';
import { ViewPrivateConversationDto } from '../dto/view-private-conversation.dto';
import { ChatPivateMessageService } from '../service/chat-private-message.service';
import { TransformInterceptor } from 'src/common/interceptor/transform.interceptor';
import { SendPrivateMessageDto } from '../dto/send-private-message.dto';
import { SendGroupMessageDto } from '../dto/send-group-message.dto';
import { ChatGroupMessageService } from '../service/chat-group-message.service';
import { ViewGroupConversationDto } from '../dto/view-group-conversation.dto';
import { Role } from 'src/auth/enums/role.enum';
import { ChatService } from '../service/chat.service';

const chatGatewayNameSpace = 'chat';

@WebSocketGateway({
  namespace: chatGatewayNameSpace,
  cors: {
    // if you are connecting the socket from any other port then you need added them in cors config.
    origin: [
      'https://hoppscotch.io',
      'http://localhost:3000',
      'http://localhost:4200',
      'http://localhost',
      'http://dev.tiggadesk.com',
      'https://dev.tiggadesk.com',
      'http://staging.tiggadesk.com',
      'https://staging.tiggadesk.com',
      'http://portal.tiggadesk.com',
      'https://portal.tiggadesk.com',
    ],
  },
})
export class ChatGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer()
  server: Server;

  constructor(
    private logger: Logger,
    private authService: AuthService,
    private connectedUserSocektService: ConnectedUserSocketService,
    private userService: UserService,
    private chatGroupService: ChatGroupService,
    private chatPivateMessageService: ChatPivateMessageService,
    private chatGroupMessageService: ChatGroupMessageService,
    private chatService: ChatService,
  ) {}

  /**
   * emit error event
   * @param socket
   * @param object
   */
  private emitError(socket: Socket, object: ChatErrorInterface) {
    socket.emit(ChatServerEvent._u_error, object);
  }
  /**
   * disconnect socket
   * @param socket
   */
  private forceDisconnect(socket: Socket) {
    socket.disconnect();
  }

  /**
   * life cycle event
   * @param server
   */
  afterInit(server: any) {
    // console.log('afterInit', server);
  }
  /**
   * verify the provided jwt and decide if to allow the socket connection or not..
   * @param socket
   */
  async handleConnection(socket: Socket) {
    try {
      // verify user and get payload
      const user: User = await this.authService.verifyUserToken(
        socket.handshake.auth.accessToken,
      );

      // add user data into socket
      socket.data.user = user;

      // join default room(i.e. userId, accountId)
      socket.join(`user-${user.id}`);
      socket.join(`account-${user.accountId}`);

      // join chat groups room
      if (Array.isArray(user.chatGroups)) {
        for (let index in user.chatGroups) {
          socket.join(user.chatGroups[index]['chatGroupId']);
        }
      }

      // save conencted socket into db
      const connectedUser: CreateConnectedUserSocketDto = {
        socket_id: socket.id,
        user: user,
      };

      await this.connectedUserSocektService.create(connectedUser);

      socket.emit(ChatServerEvent._u_after_handle_connection, user);
    } catch (error) {
      this.logger.error(error.message, error, 'ChatGateway.handleConnection');

      const chatError: ChatErrorInterface = {
        name: 'Token error',
        message: error.message,
        code: ErroCode.INVALID_TOKEN,
        status: StatusCode.UNAUTHORIZED,
        type: error.name,
      };

      this.emitError(socket, chatError);

      return this.forceDisconnect(socket);
    }
  }

  /**
   * on disconnect
   * @param socket
   */
  async handleDisconnect(socket: Socket) {
    const currentUser: User = socket.data.user;
    const accountId = currentUser.accountId;

    // remove connection from DB
    //socket.data.user
    await this.connectedUserSocektService.delete(socket.id);
    socket.disconnect();

    switch (currentUser.role) {
      case Role.ADMIN:
      case Role.AGENT:
        // borad cast into the account room
        const agents = await this.userService.getAccountAgentUsers(accountId);
        this.server
          .to(`account-${currentUser.accountId}`)
          .emit(ChatServerEvent._m_agent_offline, agents);
        break;
      case Role.GUEST:
        this.server
          .to(`account-${currentUser.accountId}`)
          .emit(ChatServerEvent._m_p_guest_offline);
        break;
    }
  }

  /**
   * init chat UI
   * @param data
   * @param socket
   * @returns
   */
  @SubscribeMessage(ChatClientEvent.load_chat_ui)
  async initChat(@MessageBody() data: any, @ConnectedSocket() socket: Socket) {
    const currentUser: User = socket.data.user;

    const response = {
      groups: await this.chatGroupService.getUserChatGroup(currentUser),
      users: await this.userService.getAccountAgentUsers(currentUser.accountId),
      guests: await this.userService.getAssignedGuestUsers(currentUser),
    };

    switch (currentUser.role) {
      case Role.ADMIN:
      case Role.AGENT:
        // borad cast into the account room
        this.server
          .to(`account-${currentUser.accountId}`)
          .emit(ChatServerEvent._m_agent_online, response.users);
        break;
      case Role.GUEST:
        break;
    }

    return response;
  }

  /**
   * fetch agents..
   * @param data
   * @param socket
   * @returns
   */
  @SubscribeMessage(ChatClientEvent.fetch_agents)
  async fetchAgents(
    @MessageBody() data: any,
    @ConnectedSocket() socket: Socket,
  ) {
    const currentUser: User = socket.data.user;
    const agents = await this.userService.getAccountAgentUsers(
      currentUser.accountId,
    );
    return agents;
  }

  /**
   * fetch chat groups
   * @param data fet
   * @param socket
   * @returns
   */
  @SubscribeMessage(ChatClientEvent.fetch_chat_groups)
  async fetchUserChatGroups(
    @MessageBody() data: any,
    @ConnectedSocket() socket: Socket,
  ) {
    const currentUser: User = socket.data.user;
    const agents = await this.chatGroupService.getUserChatGroup(currentUser);
    return agents;
  }

  /**
   * fetch chat groups
   * @param data fet
   * @param socket
   * @returns
   */
  @SubscribeMessage(ChatClientEvent.fetch_assigned_guests)
  async fetchAssignedGuestUsers(
    @MessageBody() data: any,
    @ConnectedSocket() socket: Socket,
  ) {
    const currentUser: User = socket.data.user;
    const guests = await this.userService.getAssignedGuestUsers(currentUser);

    console.log('fetch_assigned_guests', guests);
    return guests;
  }

  /**
   * init chat UI
   * @param data
   * @param socket
   * @returns
   */
  @UseInterceptors(new TransformInterceptor()) // to hide the the property marked as @Exclude({ toPlainOnly: true }) in socket response..
  @SubscribeMessage(ChatClientEvent.view_private_conversation)
  @UsePipes(new ValidationPipe({ transform: true })) // use dto validaton and use transform to apply default valude defiend on dto
  async viewPrivateConversation(
    @MessageBody() viewPrivateConversationDto: ViewPrivateConversationDto,
    @ConnectedSocket() socket: Socket,
  ) {
    const { patnerUserId, page, limit } = viewPrivateConversationDto;

    const currentUserId: string = socket.data.user.id;

    const paginateOptions = {
      page,
      limit,
      route: '',
    };

    const list = await this.chatPivateMessageService.getPrivateConversation(
      currentUserId,
      patnerUserId,
      paginateOptions,
    );

    return list;
  }

  /**
   * init chat UI
   * @param data
   * @param socket
   * @returns
   */
  @UseInterceptors(new TransformInterceptor()) // to hide the the property marked as @Exclude({ toPlainOnly: true }) in socket response..
  @SubscribeMessage(ChatClientEvent.send_private_message)
  async sendPrivateMessage(
    @MessageBody() sendPrivateMessageDto: SendPrivateMessageDto,
    @ConnectedSocket() socket: Socket,
  ) {
    // replace sentById with user id from socket, to make it secure..
    sendPrivateMessageDto.sentById = socket.data.user.id;

    const sentMessage = await this.chatPivateMessageService.sendPrivateMessage(
      sendPrivateMessageDto,
    );

    // to all clients in room1 and/or room2 except the sender
    this.server
      .to(`user-${sendPrivateMessageDto.sentToId}`)
      .to(`user-${sendPrivateMessageDto.sentById}`)
      .emit(ChatServerEvent._u_private_message_received, sentMessage);

    return sentMessage;
  }

  @UseInterceptors(new TransformInterceptor()) // to hide the the property marked as @Exclude({ toPlainOnly: true }) in socket response..
  @SubscribeMessage(ChatClientEvent.view_group_conversation)
  @UsePipes(new ValidationPipe({ transform: true })) // use dto validaton and use transform to apply default valude defiend on dto
  async viewGroupConversation(
    @MessageBody() viewGroupConversationDto: ViewGroupConversationDto,
    @ConnectedSocket() socket: Socket,
  ) {
    const { groupId, page, limit } = viewGroupConversationDto;

    const currentUserId: string = socket.data.user.id;
    // @check if current users belongs to requested group..

    const paginateOptions = {
      page,
      limit,
      route: '',
    };

    const list = await this.chatGroupMessageService.getGroupConversation(
      groupId,
      paginateOptions,
    );

    return list;
  }

  @UseInterceptors(new TransformInterceptor()) // to hide the the property marked as @Exclude({ toPlainOnly: true }) in socket response..
  @SubscribeMessage(ChatClientEvent.send_group_message)
  async sendGroupMessage(
    @MessageBody() sendGroupMessageDto: SendGroupMessageDto,
    @ConnectedSocket() socket: Socket,
  ) {
    // replace sentById with user id from socket, to make it secure..
    sendGroupMessageDto.sentById = socket.data.user.id;

    const sentMessage = await this.chatGroupMessageService.sendGroupMessage(
      sendGroupMessageDto,
    );

    this.server
      .to(`${sendGroupMessageDto.sentToGroupId}`)
      .emit(ChatServerEvent._m_group_message_received, sentMessage);

    return sentMessage;
  }


  //
  @SubscribeMessage(ChatClientEvent.agent_ended_chat)
  async agentEndedChat(
    @MessageBody() data: any,
    @ConnectedSocket() socket: Socket,
  ) {

    const guestUserId:string = data.patnerUserId;

    const agentUser: User = socket.data.user;

    await this.userService.update(
      guestUserId,
      { servedById: null },
      agentUser,
    );

    this.server
    .to(`user-${guestUserId}`)
    .emit(ChatServerEvent._u_agent_ended_chat, {});

    return {};
  }


  /********************************* guest specific listeners ***************************************/

  /**
   * init chat UI
   * @param data
   * @param socket
   * @returns
   */
  @SubscribeMessage(ChatClientEvent.load_chat_widget_ui)
  async initChatWidget(
    @MessageBody() data: any,
    @ConnectedSocket() socket: Socket,
  ) {
    const currentUser: User = socket.data.user;

    const response = {};

    switch (currentUser.role) {
      case Role.GUEST:
        this.server
          .to(`account-${currentUser.accountId}`)
          .emit(ChatServerEvent._m_p_guest_online, response);
        break;
    }

    return response;
  }

  /**
   * todo check if request is from quest user...
   * @param socket guest requset agent
   * @returns
   */
  @UseInterceptors(new TransformInterceptor()) // to hide the the property marked as @Exclude({ toPlainOnly: true }) in socket response..
  @SubscribeMessage(ChatClientEvent.guest_request_agent)
  async guestRequestAgent(@ConnectedSocket() socket: Socket) {
    const currentGuestUser: User = socket.data.user;

    const params = JSON.parse(currentGuestUser.params);

    if (params.preferred_users.length > 0) {
      const agentsPicked = await this.userService.pickAgent(
        params.preferred_users,
      );

      if (Array.isArray(agentsPicked) && agentsPicked.length > 0) {
        const agentPicked = agentsPicked[0];

        const ticket = await this.chatService.assignTicket(
          agentPicked.id,
          currentGuestUser.last_ticket_id,
        );

        /**
         * once ticket is assigned to the agent successfully....
         */
        ticket.subscribe(async(data)=>{

          if(data){

            await this.userService.update(
              currentGuestUser.id,
              { servedById: agentPicked.id },
              currentGuestUser,
            );
    
            // notify guest
            this.server
              .to(`user-${currentGuestUser.id}`)
              .emit(ChatServerEvent._u_guest_assigned_agent, agentPicked);
    
            // notify agent
            this.server
              .to(`user-${agentPicked.id}`)
              .emit(ChatServerEvent._m_p_guest_online);

          } else {

            const chatError: ChatErrorInterface = {
              name: 'Ticket assignment failed',
              message:  'Ticket assignment failed',
              code: ErroCode.FAILED_TO_ASSIGN_TICKET,
              status: StatusCode.INTERNAL_SERVER_ERROR,
              type: 'HTTP_SERVICES',
            };
      
            this.emitError(socket, chatError);
            
            throw new WsException('Ticket could not be assigned');
          }
         
        });


     

        return agentPicked;
      }
    }
  }
}
