import { IoAdapter } from '@nestjs/platform-socket.io';
import { RedisClient } from 'redis';
import { ServerOptions } from 'socket.io';
import { createAdapter } from 'socket.io-redis';
import { ConfigModule, ConfigService } from '@nestjs/config';



const configService = new ConfigService();

const pubClient = new RedisClient({ host: configService.get('REDIS_HOST'), port: configService.get('REDIS_PORT') });
const subClient = pubClient.duplicate();

const redisAdapter = createAdapter({ pubClient, subClient });




export class RedisIoAdapter extends IoAdapter {
  createIOServer(port: number, options?: ServerOptions): any {
    const server = super.createIOServer(port, options);
    server.adapter(redisAdapter);
    return server;
  }
}