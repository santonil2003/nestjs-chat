import {
  Connection,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm';
import { CreateChatGroupsUsersDto } from '../dto/create-chat-groups-users-dto';
import { ChatGroup } from '../entity/chat-group.entity';
import { ChatGroupsUsersService } from '../service/chat-groups-users.service';

@EventSubscriber()
export class ChatGroupSubscriber
  implements EntitySubscriberInterface<ChatGroup>
{
  constructor(
    connection: Connection,
    private readonly chatGroupsUsersService: ChatGroupsUsersService,
  ) {
    connection.subscribers.push(this);
  }

  /**
   * Indicates that this subscriber only listen to Post events.
   */
  listenTo() {
    return ChatGroup;
  }

  /**
   * Called after entity insertion.
   */
  afterInsert(event: InsertEvent<any>) {
    const chatGroup: ChatGroup = event.entity;

    const createChatGroupsUsersDto: CreateChatGroupsUsersDto = {
        chatGroupId: chatGroup.id,
        userId: chatGroup.groupOwnerId,
        params: null
    }

    this.chatGroupsUsersService.create(createChatGroupsUsersDto);

    console.log(`AFTER ENTITY INSERTED: `, event.entity);
  }
}
