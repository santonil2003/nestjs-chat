export interface ChatErrorInterface {
    name: string;
    message:string;
    code:number;
    status:number;
    type:string;
}
