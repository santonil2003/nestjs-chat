import { ChatGroup } from 'src/chat/entity/chat-group.entity';

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ChatMessage } from './chat-message.entity';

@Entity({ name: 'tigga_chat_group_message' })
export class ChatGroupMessage {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  sentToGroupId: string;
  /**
   * desitnation group
   */
  @ManyToOne((type) => ChatGroup, (group) => group.chatGroupMessages)
  sentToGroup: ChatGroup;


  @Column({ nullable: true })
  chatMessageId: string;

  @OneToOne(() => ChatMessage, { eager: true })
  @JoinColumn()
  chatMessage: ChatMessage;
}
