
import { User } from "src/auth/entity/user.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";


@Entity({name: 'tigga_chat_connected_user_socket'})
export class ConnectedUserSocket{

  @PrimaryGeneratedColumn('uuid')
  id: string;
  
  @Column()
  socket_id: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @ManyToOne((type) => User, (user) => user.connectedUserSockets, { eager: false })
  user: User;
}