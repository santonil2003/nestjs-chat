import { User } from 'src/auth/entity/user.entity';
import { ChatGroup } from 'src/chat/entity/chat-group.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ChatMessageAttachment } from './chat-message-attachment.entity';
import { SeenChatMessage } from './seen-chat-message.entity';

@Entity({ name: 'tigga_chat_message' })
export class ChatMessage {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'text' })
  content: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;


  @Column({ nullable: true })
  sentById: string;

  /**
   * sender
   */
  @ManyToOne((type) => User, (user) => user.chatMessages)
  sentBy: User;


  /**
   * seen by users
   */
  @OneToMany(type => SeenChatMessage, seenChatMessage => seenChatMessage.chatMessage)
  seenBy: SeenChatMessage[];



  @OneToMany((type) => ChatMessageAttachment, chatMessageAttachment => chatMessageAttachment.chatMessage)
  attachments: ChatMessageAttachment[];

}
