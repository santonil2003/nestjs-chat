import { User } from 'src/auth/entity/user.entity';
import { ChatGroup } from 'src/chat/entity/chat-group.entity';

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ChatMessage } from './chat-message.entity';

@Entity({ name: 'tigga_chat_private_message' })
export class ChatPrivateMessage {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    ticket_id: string;
  
    @Column({ nullable: true })
    sentToId: string;

    @ManyToOne((type) => User, (user) => user.chatPrivateMessages)
    sentTo: User;

    @Column({ nullable: true })
    chatMessageId: string;

    @OneToOne((type) => ChatMessage, { eager: true})
    @JoinColumn()
    chatMessage: ChatMessage;
}
