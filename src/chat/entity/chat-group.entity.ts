import { Account } from 'src/account/entity/account.entity';
import { User } from 'src/auth/entity/user.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ChatGroupMessage } from './chat-group-message.entity';
import { ChatGroupsUsers } from './chat-groups-users.entity';

@Entity({ name: 'tigga_chat_group' })
export class ChatGroup {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  display_name: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  groupOwnerId: string;

  @ManyToOne((type) => User, (user) => user.myGroups)
  groupOwner: User;

  @Column({type:"text", nullable: true })
  params: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;


  @Column({ nullable: true })
  accountId: string;

  @ManyToOne((type) => Account, (account) => account.groups)
  account: Account;


  @OneToMany((type) => ChatGroupMessage, (chatGroupMessage) => chatGroupMessage.sentToGroup)
  chatGroupMessages: ChatGroupMessage[];
  

  
  /**
   * belongs to one or more group
   */
   @OneToMany(
    (type) => ChatGroupsUsers,
    (chatGroupsUsers) => chatGroupsUsers.chatGroup,
  )
  groupUsers: ChatGroupsUsers[];
}
