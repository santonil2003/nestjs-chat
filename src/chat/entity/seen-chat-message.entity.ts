
import { User } from 'src/auth/entity/user.entity';
import { ChatMessage } from 'src/chat/entity/chat-message.entity';

import {
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'tigga_chat_seen_message' })
export class SeenChatMessage {

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @ManyToOne(type => ChatMessage, chatMessage => chatMessage.seenBy, { primary: true })
  chatMessage: ChatMessage;

  @ManyToOne(type => User, user => user.seenChatMessages, { primary: true })
  seenBy: User;

}
