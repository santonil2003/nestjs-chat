import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { User } from 'src/auth/entity/user.entity';

import { ChatGroup } from './chat-group.entity';

@Entity({ name: 'tigga_chat_groups_users' })
export class ChatGroupsUsers {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({type:"text", nullable: true })
  params: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;


  @Column({ nullable: true })
  chatGroupId: string;

  @ManyToOne(type => ChatGroup, chatGroup => chatGroup.groupUsers, { primary: true })
  chatGroup: ChatGroup;

  @Column({ nullable: true })
  userId: string;
  
  @ManyToOne(type => User, user => user.chatGroups, { primary: true })
  user: User;


}
