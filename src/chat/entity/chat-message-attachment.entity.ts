
import { ChatMessage } from 'src/chat/entity/chat-message.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';


@Entity({ name: 'tigga_chat_message_attachment' })

export class ChatMessageAttachment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 255 })
  originalname: string;

  @Column('varchar', { length: 255 })
  filename: string;

  @Column()
  mimetype: string;

  @Column('varchar', { length: 255 })
  path: string;

  @Column({type: "float"})
  size: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  /**
   * belongs to chat
   */
  @ManyToOne((type) => ChatMessage, (chatMessage) => chatMessage.attachments)
  chatMessage: ChatMessage;

}
