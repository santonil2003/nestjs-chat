import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { SeenChatMessage } from "../entity/seen-chat-message.entity";


@Injectable()
export class SeenChatMessagesService {
  constructor(
    @InjectRepository(SeenChatMessage)
    private readonly chatMessageRepository: Repository<SeenChatMessage>,
  ) {}

}