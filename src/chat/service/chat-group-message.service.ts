import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { WsException } from "@nestjs/websockets";
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import { Repository } from "typeorm";
import { SendGroupMessageDto } from "../dto/send-group-message.dto";
import { ChatGroupMessage } from "../entity/chat-group-message.entity";
import { ChatMessageService } from "./chat-message.service";


@Injectable()
export class ChatGroupMessageService {
  constructor(
    @InjectRepository(ChatGroupMessage)
    private readonly chatGroupMessageRepository: Repository<ChatGroupMessage>,
    private readonly chatMessageService: ChatMessageService,
  ) {}


  /**
   * get private conversation between two user
   * @param currentUserId
   * @param patnerUserId
   * @param options
   * @returns
   */
   async getGroupConversation(
    groupId: string,
    options: IPaginationOptions,
  ): Promise<Pagination<ChatGroupMessage>> {
    const query = this.chatGroupMessageRepository.createQueryBuilder('crm');

    query.addSelect(['chatMessage', 'sentBy', 'sentToGroup']);

    query.innerJoin('crm.chatMessage', 'chatMessage');

    query.innerJoin('crm.sentToGroup', 'sentToGroup');

    query.innerJoin('chatMessage.sentBy', 'sentBy');

    query.where(
      '( crm.sentToGroup = :groupId )',
      { groupId: groupId },
    );

    query.orderBy('chatMessage.created_at', 'DESC');

    return paginate<ChatGroupMessage>(query, options);
  }

  /**
   * get one private message
   * @param id 
   * @returns 
   */
  async getGroupMessage(id: string): Promise<ChatGroupMessage> {

    const query = this.chatGroupMessageRepository.createQueryBuilder('crm');

    query.addSelect(['chatMessage', 'sentBy', 'sentToGroup']);

    query.innerJoin('crm.chatMessage', 'chatMessage');

    query.innerJoin('crm.sentToGroup', 'sentToGroup');

    query.innerJoin('chatMessage.sentBy', 'sentBy');

    query.where('crm.id = :id', {id:id});

    return await query.getOne();
  }





  async sendGroupMessage(
    sendGroupMessageDto: SendGroupMessageDto
  ): Promise<ChatGroupMessage> {

    const {sentById, 	sentToGroupId, content} = sendGroupMessageDto;

    const chatMessage = await this.chatMessageService.create({
      sentById,
      content
    });

    if(!chatMessage){
      throw new WsException(`Chat message could not be saved.`);
    }

    const groupMessage = this.chatGroupMessageRepository.create({
      sentToGroupId,
      chatMessageId: chatMessage.id
    });

    await this.chatGroupMessageRepository.save(groupMessage);


    return await this.getGroupMessage(groupMessage.id);


  }


}