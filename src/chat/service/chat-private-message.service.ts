import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WsException } from '@nestjs/websockets';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { getManager, Repository } from 'typeorm';
import { SendPrivateMessageDto } from '../dto/send-private-message.dto';
import { ChatPrivateMessage } from '../entity/chat-private-message.entity';
import { ChatMessageService } from './chat-message.service';

@Injectable()
export class ChatPivateMessageService {
  constructor(
    @InjectRepository(ChatPrivateMessage)
    private readonly chatPrivateMessageRepository: Repository<ChatPrivateMessage>,
    private readonly chatMessageService: ChatMessageService,
  ) {}

  /**
   * get private conversation between two user
   * @param currentUserId
   * @param patnerUserId
   * @param options
   * @returns
   */
  async getPrivateConversation(
    currentUserId: string,
    patnerUserId: string,
    options: IPaginationOptions,
  ): Promise<Pagination<ChatPrivateMessage>> {
    const query = this.chatPrivateMessageRepository.createQueryBuilder('cpm');

    query.addSelect(['chatMessage', 'sentBy', 'sentTo']);

    query.innerJoin('cpm.chatMessage', 'chatMessage');

    query.innerJoin('cpm.sentTo', 'sentTo');

    query.innerJoin('chatMessage.sentBy', 'sentBy');

    query.where(
      '( cpm.sentToId = :currentUserId AND chatMessage.sentById = :patnerUserId )',
      { currentUserId: currentUserId, patnerUserId: patnerUserId },
    );

    query.orWhere(
      '( cpm.sentToId = :patnerUserId AND chatMessage.sentById = :currentUserId )',
      { currentUserId: currentUserId, patnerUserId: patnerUserId },
    );

    query.orderBy('chatMessage.created_at', 'DESC');

    return paginate<ChatPrivateMessage>(query, options);
  }

  /**
   * get private conversation between two user
   * @param currentUserId
   * @param patnerUserId
   * @param options
   * @returns
   */
  async getGuestConversation(
    guestUserId: string,
    options: IPaginationOptions,
  ): Promise<Pagination<ChatPrivateMessage>> {
    const query = this.chatPrivateMessageRepository.createQueryBuilder('cpm');

    query.addSelect(['chatMessage', 'sentBy']);

    query.innerJoin('cpm.chatMessage', 'chatMessage');

    query.innerJoin('chatMessage.sentBy', 'sentBy');

    query.where(
      '( cpm.sentToId = :guestUserId OR chatMessage.sentById = :guestUserId )',
      { guestUserId: guestUserId },
    );

    query.orderBy('chatMessage.created_at', 'DESC');

    return paginate<ChatPrivateMessage>(query, options);
  }

  /**
   * get one private message
   * @param id
   * @returns
   */
  async getPrivateMessage(id: string): Promise<ChatPrivateMessage> {
    const query = this.chatPrivateMessageRepository.createQueryBuilder('cpm');

    query.addSelect(['chatMessage', 'sentBy', 'sentTo']);

    query.innerJoin('cpm.chatMessage', 'chatMessage');

    query.innerJoin('cpm.sentTo', 'sentTo');

    query.innerJoin('chatMessage.sentBy', 'sentBy');

    query.where('cpm.id = :id', { id: id });

    return await query.getOne();
  }

  /**
   * get one private message
   * @param id
   * @returns
   */
     async getGuestPrivateMessage(id: string): Promise<ChatPrivateMessage> {

      const query = this.chatPrivateMessageRepository.createQueryBuilder('cpm');
  
      query.addSelect(['chatMessage', 'sentBy']);
  
      query.innerJoin('cpm.chatMessage', 'chatMessage');
  
      query.innerJoin('chatMessage.sentBy', 'sentBy');
  
      query.where('cpm.id = :id', { id: id });
  
      return await query.getOne();
    }

  /**
   * send private message
   * @param sendPrivateMessageDto
   * @returns
   */
  async sendPrivateMessage(
    sendPrivateMessageDto: SendPrivateMessageDto,
  ): Promise<ChatPrivateMessage> {
    const { sentById, sentToId, content } = sendPrivateMessageDto;

    const chatMessage = await this.chatMessageService.create({
      sentById,
      content,
    });

    if (!chatMessage) {
      throw new WsException(`Chat message could not be saved.`);
    }

    const privateMessage = this.chatPrivateMessageRepository.create({
      sentToId,
      chatMessageId: chatMessage.id,
    });

    await this.chatPrivateMessageRepository.save(privateMessage);

    return await this.getPrivateMessage(privateMessage.id);
  }

}
