import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/entity/user.entity';
import { getManager, Repository } from 'typeorm';
import { CreateChatGroupsUsersDto } from '../dto/create-chat-groups-users-dto';
import { ChatGroupsUsers } from '../entity/chat-groups-users.entity';

@Injectable()
export class ChatGroupsUsersService {
  constructor(
    @InjectRepository(ChatGroupsUsers)
    private readonly chatGroupsUsersRepository: Repository<ChatGroupsUsers>,
  ) {}

  /**
   * get user chat group
   * @param currentUser
   * @returns
   */
  async getUserChatGroup(user: User) {
    const sql = `SELECT 
                    tcg.*
                FROM
                    tigga_chat_group AS tcg 
                JOIN 
                    tigga_chat_groups_users as tcgu ON tcgu.chatGroupId = tcg.id
                WHERE
                    tcgu.userId = ?
                AND 
                    tcg.accountId = ?
                        AND tcg.deleted_at IS NULL AND tcgu.deleted_at IS NULL`;

    return await getManager().query(sql, [user.id, user.account.id]);
  }

  /**
   * create chat groups users
   * @param createChatGroupDto 
   * @returns 
   */
  async create(createChatGroupsUsersDto: CreateChatGroupsUsersDto) : Promise<ChatGroupsUsers>{
    try {
      const {
        chatGroupId,
        userId,
        params,
      } = createChatGroupsUsersDto;

      const model = this.chatGroupsUsersRepository.create({
        chatGroupId,
        userId,
        params,
      });

      await this.chatGroupsUsersRepository.save(model);

      return model;
    } catch (error) {
      switch (error.errno) {
        case 1062:
          throw new ConflictException(`${error.message}`);
        default:
          throw new InternalServerErrorException(error.message);
      }
    }
  }
}
