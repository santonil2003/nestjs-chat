import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/entity/user.entity';
import { getManager, Repository } from 'typeorm';
import { CreateChatGroupDto } from '../dto/create-chat-group-dto';
import { ChatGroup } from '../entity/chat-group.entity';

@Injectable()
export class ChatGroupService {
  constructor(
    @InjectRepository(ChatGroup)
    private readonly chatGroupRepository: Repository<ChatGroup>,
  ) {}

  /**
   * get user chat group
   * @param currentUser
   * @returns
   */
  async getUserChatGroup(user: User) {
    const sql = `SELECT 
                    tcg.*
                FROM
                    tigga_chat_group AS tcg 
                JOIN 
                    tigga_chat_groups_users as tcgu ON tcgu.chatGroupId = tcg.id
                WHERE
                    tcgu.userId = ?
                AND 
                    tcg.accountId = ?
                        AND tcg.deleted_at IS NULL AND tcgu.deleted_at IS NULL`;

    return await getManager().query(sql, [user.id, user.account.id]);
  }

  async getAllChatGroup(user: User) {
    const sql = `SELECT 
                      tcr.*
                  FROM
                      tigga_chat_group AS tcr
                  WHERE
                      tcr.accountId = ?
                          AND deleted_at IS NULL`;

    return await getManager().query(sql, [user.account.id]);
  }

  /**
   * create chat group
   * @param createChatGroupDto 
   * @returns 
   */
  async create(createChatGroupDto: CreateChatGroupDto) : Promise<ChatGroup>{
    try {
      const {
        name,
        display_name,
        description,
        groupOwnerId,
        params,
        accountId,
      } = createChatGroupDto;

      const model = this.chatGroupRepository.create({
        name,
        display_name,
        description,
        groupOwnerId,
        params,
        accountId,
      });

      await this.chatGroupRepository.save(model);

      return model;
    } catch (error) {
      switch (error.errno) {
        case 1062:
          throw new ConflictException(`${error.message}`);
        default:
          throw new InternalServerErrorException(error.message);
      }
    }
  }
}
