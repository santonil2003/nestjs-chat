import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ChatMessageAttachment } from "../entity/chat-message-attachment.entity";


@Injectable()
export class ChatMessageAttachmentService {
  constructor(
    @InjectRepository(ChatMessageAttachment)
    private readonly chatMessageAttachmentRepository: Repository<ChatMessageAttachment>,
  ) {}

}