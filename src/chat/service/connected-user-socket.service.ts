import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateConnectedUserSocketDto } from '../dto/create-connected-user-socket.dto';
import { ConnectedUserSocket } from '../entity/connected-user-socket.entity';

@Injectable()
export class ConnectedUserSocketService {
  constructor(
    @InjectRepository(ConnectedUserSocket)
    private readonly connectedUserSocketRepository: Repository<ConnectedUserSocket>,
  ) {}

  async create(
    createConnectedUserSocketDto: CreateConnectedUserSocketDto,
  ): Promise<ConnectedUserSocket> {


    const {socket_id, user} = createConnectedUserSocketDto;

    const data = {
      socket_id,
      user
    };

    const newConnectedUser = this.connectedUserSocketRepository.create(data);

    await this.connectedUserSocketRepository.save(newConnectedUser);

    return newConnectedUser;


  }




  async delete(socketId: string) {

    const result = await this.connectedUserSocketRepository.delete({
      socket_id: socketId,
    });

    if (result.affected === 0) {
      // nothing to delete...
    }

    return;
  } 
}