import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateChatMessageDto } from "../dto/create-chat-message.dto";
import { ChatMessage } from "../entity/chat-message.entity";


@Injectable()
export class ChatMessageService {
  constructor(
    @InjectRepository(ChatMessage)
    private readonly chatMessageRepository: Repository<ChatMessage>,
  ) {}
  
  /**
   * create
   * @param createChatMessageDto
   * @returns 
   */
  async create(
    createChatMessageDto: CreateChatMessageDto
  ): Promise<ChatMessage> {

    const {sentById, content} = createChatMessageDto;

    const chatMessage = this.chatMessageRepository.create({
      content,
      sentById
    });

    await this.chatMessageRepository.save(chatMessage);

    return chatMessage;
  }
}