import { HttpService } from '@nestjs/axios';
import {
    Injectable,
  } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, map, tap } from 'rxjs';
import { User } from 'src/auth/entity/user.entity';
import { UserService } from 'src/auth/service/user.service';
import { AppService } from 'src/common/service/app.service';

  @Injectable()
  export class ChatService {

    constructor(
        private userService: UserService,
        private httpService: HttpService,
        private configService: ConfigService,
        private appService: AppService
      ) {}

    /**
     * get account users
     * @param currentUser 
     * @returns 
     */
    async getAccountAgentUsers(currentUser: User) {
        return await this.userService.getAccountAgentUsers(currentUser.accountId);
    }


    /**
     * assign ticket
     * @param userId
     * @param ticketId 
     * @returns 
     */
    async assignTicket(userId: string, ticketId: string): Promise<any>{

      const apiBaseUrl = await this.configService.get('TIGGADESK_API_BASE_URL');

      const url = apiBaseUrl+'/v1/chat-request/assign-ticket';

      console.log('url',url);

      const data = {
        chat_user_id: userId,
        ticket_id: ticketId
      };

      const result = this.httpService.post(url, data).pipe(
        map((response) => {
          return response.data
        }),
        catchError(this.appService.handleHttpServiceError(`ChatService.assignTicket`, false))
      );
  
      return result;
      
    }

    

  

  }