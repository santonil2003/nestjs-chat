import { InjectQueue } from '@nestjs/bull';
import { Controller, Get, Logger } from '@nestjs/common';
import { Queue } from 'bull';
import { AppService } from './common/service/app.service';
import { EmailProducerService } from './common/service/email.producer.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly logger: Logger,
    private readonly emailProducerService: EmailProducerService,
    @InjectQueue('file-queue') private fileQueue: Queue,
  ) {}

  @Get()
  getHello(): string {
    this.logger.log('Hi this is log', 'getHello', 'AppController');
    return this.appService.getHello();
  }

  @Get('send-email')
  sendEmail() {
    this.emailProducerService.sendEmail('hi how are you there');
  }

  @Get('push-job-to-queue')
  pushJobToQueue() {
    this.fileQueue.add(
      'unlink-file-job',
      {
        id: '1234',
        path: 'bla bla bla',
        datetime: new Date(),
      },
      {
        delay: 5000,
      },
    );
  }
}
