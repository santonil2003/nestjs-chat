import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../entity/user.entity';
import { UserRepository } from '../repository/users.repository';
import * as bcrypt from 'bcrypt';
import { LoginUserDto } from '../dto/login-user.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../interface/jwt-payload.interface';
import { LoginSpecialUserDto } from '../dto/login-speical-user.dto';
import { AccountService } from 'src/account/service/account/account.service';
import { Account } from 'src/account/entity/account.entity';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private accountService: AccountService,
    private jwtService: JwtService,
  ) {}

  /**
   * login 
   * @param loginUserDto 
   * @returns 
   */
  async logIn(loginUserDto: LoginUserDto): Promise<{accessToken:string}> {
      const {username, password} = loginUserDto;

      const user = await this.userRepository.findOne({username:username});

      console.log(user.password);

      if(user && await bcrypt.compare(password, user.password)){
        // generate jwt
        const payload: JwtPayload  = {id:user.id, username:user.username};
        const accessToken:string = await this.jwtService.sign(payload);
        return {accessToken};
      }

      throw new UnauthorizedException('Invalid username or passowrd.');

  }

    /**
   * login 
   * @param loginUserDto 
   * @returns 
   */
     async logInSpecialUser(loginSpecialUserDto: LoginSpecialUserDto): Promise<{accessToken:string}> {

      const {accountId, app_key, userId} = loginSpecialUserDto;

      const account: Account =  await this.accountService.findOneByIdAndAppKey(accountId, app_key);

      if(account){

        const user: User = await this.userRepository.findOne({id: userId});

        if(user){
          // generate jwt
          const payload: JwtPayload  = {id:user.id, username:user.username};
          const accessToken:string = await this.jwtService.sign(payload);
          return {accessToken};
        }
  
        
      }

      throw new NotFoundException(`Invalid account.`);

  }

  /**
   * verify jwt
   * @param jwt
   * @returns 
   */
  verifyJwt(jwt: string): Promise<any> {
    return this.jwtService.verifyAsync(jwt);
  }

  /**
   * verify user token
   * @param jwt 
   * @returns 
   */
  async verifyUserToken(jwt:string): Promise<User>{

    return new Promise(async (resolve, reject) => {
      await this.verifyJwt(jwt)
        .then(
          async (payload: JwtPayload) => {

            const user: User = await this.getOne(payload.id);

            if (user) {
              resolve(user);
            }

            throw new Error('User not found.');
          },
          (error) => {
            reject(error);
          },
        )
        .catch((error) => {
          reject(error);
        });
    });
  }


  async getUser(id: string): Promise<User>{
    return await this.userRepository.findOne({id});
  }

  async getOne(id: string): Promise<User> {
    return await this.userRepository.findOneOrFail({ id });
  }

}
