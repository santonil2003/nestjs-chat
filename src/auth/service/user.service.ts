import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { AccountService } from 'src/account/service/account/account.service';
import { getManager } from 'typeorm';
import { GetUser } from '../decorator/get-user.decorator';
import { CreateSpecialUserDto } from '../dto/create-special-user.dto';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../entity/user.entity';
import { Role } from '../enums/role.enum';
import { UserRepository } from '../repository/users.repository';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository) private userRepository: UserRepository,
    private readonly accountService: AccountService,
  ) {}

  async createSpecialUser(createUserDto: CreateSpecialUserDto): Promise<User> {
    let model = {};

    User.getSafeAttributes(true).forEach((attribute) => {
      if (createUserDto.hasOwnProperty(attribute)) {
        model[attribute] = createUserDto[attribute];
      }
    });

    if (createUserDto.password) {
      const salt = await bcrypt.genSalt();
      const hashedPassword = await bcrypt.hash(createUserDto.password, salt);
      model['password'] = hashedPassword;
    }

    if (!createUserDto.username) {
      model['username'] = createUserDto.email;
    }

    try {
      const user = this.userRepository.create(model);

      await this.userRepository.save(user);

      return user;
    } catch (error) {
      switch (error.errno) {
        case 1062:
          throw new ConflictException('Username already exist');
        default:
          throw new InternalServerErrorException(error.message);
      }
    }
  }

  /**
   * create user
   * @param createUserDto 
   * @returns 
   */
  async create(createUserDto: CreateUserDto): Promise<User> {
    let model = {};

    User.getSafeAttributes(true).forEach((attribute) => {
      if (createUserDto.hasOwnProperty(attribute)) {
        model[attribute] = createUserDto[attribute];
      }
    });

    if (createUserDto.password) {
      const salt = await bcrypt.genSalt();
      const hashedPassword = await bcrypt.hash(createUserDto.password, salt);
      model['password'] = hashedPassword;
    }

    if (!createUserDto.username) {
      model['username'] = createUserDto.email;
    }

    const account = await this.accountService.findOneByIdAndAppKey(
      createUserDto.accountId,
      createUserDto.appKey,
    );

    try {
      const user = this.userRepository.create(model);

      user.account = account; // link user with account

      await this.userRepository.save(user);

      return user;
    } catch (error) {
      switch (error.errno) {
        case 1062:
          throw new ConflictException('Username already exist');
        default:
          throw new InternalServerErrorException(error.message);
      }
    }
  }

  async findAll(user: User, accountId?: string): Promise<User[]> {
    const query = this.userRepository.createQueryBuilder('user');

    // for super admin..
    if ([Role.SUPER_ADMIN].includes(user.role)) {
      if (accountId) {
        let account = await this.accountService.findOne(accountId, user);
        query.where({ account });
      }
    } else {
      let account = await this.accountService.findOne(user.account.id, user);
      query.where({ account });
    }

    query.orderBy('user.firstname', 'ASC');

    return await query.getMany();
  }

  async findOne(id: string, user: User): Promise<User> {
    try {
      if ([Role.SUPER_ADMIN].includes(user.role)) {
        return await this.userRepository.findOneOrFail({ id });
      } else {
        return await this.userRepository.findOneOrFail({
          id: id,
          account: user.account,
        });
      }
    } catch (error) {
      throw new NotFoundException(`User not found.`);
    }
  }

  /**
   * update
   * @param id
   * @param updateUserDto
   * @returns
   */
  async update(
    id: string,
    updateUserDto: UpdateUserDto,
    user: User,
  ): Promise<User> {
    const model = await this.findOne(id, user);

    User.getSafeAttributes().forEach((attribute) => {
      if (
        model.hasOwnProperty(attribute) &&
        updateUserDto.hasOwnProperty(attribute)
      ) {
        model[attribute] = updateUserDto[attribute];
      }
    });

    if (updateUserDto.password) {
      const salt = await bcrypt.genSalt();
      const hashedPassword = await bcrypt.hash(updateUserDto.password, salt);
      model.password = hashedPassword;
    }

    await this.userRepository.save(model);

    return model;
  }

  async remove(id: string, user: User): Promise<User> {
    const model = await this.findOne(id, user);
    await this.userRepository.softRemove(model);
    return model;
  }

  async restore(id: string, user: User): Promise<User> {
    await this.userRepository.restore(id);
    const model = await this.findOne(id, user);
    return model;
  }

  /**
   * get account account users based on currentUser
   * @param currentUser
   * @returns
   */
  async getAccountAgentUsers(accountId: string) {
    const sql = `SELECT 
                    tcu.id,
                    tcu.user_id,
                    tcu.firstname,
                    tcu.lastname,
                    tcu.country_code,
                    tcu.phone,
                    tcu.email,
                    tcu.username,
                    tcu.role,
                    tcu.accountId,
                    IF((SELECT 
                            COUNT(*) AS nos
                        FROM
                            tigga_chat_connected_user_socket AS tccus
                        WHERE
                            tccus.userId = tcu.id
                                AND tccus.deleted_at IS NULL), 1, 0) AS is_online
                FROM
                    tigga_chat_user AS tcu
                WHERE 
                    tcu.accountId = ? AND tcu.role IN ('ADMIN','AGENT') ORDER BY 11 DESC, 3 ASC`;

    return await getManager().query(sql, [accountId]);
  }

  async getAccountGuestUsers(accountId: string) {
    const sql = `SELECT 
                    tcu.id,
                    tcu.user_id,
                    tcu.firstname,
                    tcu.lastname,
                    tcu.country_code,
                    tcu.phone,
                    tcu.email,
                    tcu.username,
                    tcu.role,
                    tcu.accountId,
                    tcu.last_ticket_id,
                    IF((SELECT 
                            COUNT(*) AS nos
                        FROM
                            tigga_chat_connected_user_socket AS tccus
                        WHERE
                            tccus.userId = tcu.id
                                AND tccus.deleted_at IS NULL), 1, 0) AS is_online
                FROM
                    tigga_chat_user AS tcu
                WHERE 
                    tcu.accountId = ? AND tcu.role IN ('GUEST') ORDER BY tcu.id DESC`;

    return await getManager().query(sql, [accountId]);
  }

  /**
   * get assigned guest user
   * @param currentUser 
   * @returns 
   */
  async getAssignedGuestUsers(currentUser: User) {
    const sql = `SELECT 
                    tcu.id,
                    tcu.user_id,
                    tcu.firstname,
                    tcu.lastname,
                    tcu.country_code,
                    tcu.phone,
                    tcu.email,
                    tcu.username,
                    tcu.role,
                    tcu.accountId,
                    tcu.last_ticket_id,
                    IF((SELECT 
                            COUNT(*) AS nos
                        FROM
                            tigga_chat_connected_user_socket AS tccus
                        WHERE
                            tccus.userId = tcu.id
                                AND tccus.deleted_at IS NULL), 1, 0) AS is_online
                FROM
                    tigga_chat_user AS tcu
                WHERE 
                    tcu.accountId = ? AND tcu.servedById = ? AND tcu.role IN ('GUEST') ORDER BY tcu.id DESC`;

    return await getManager().query(sql, [currentUser.accountId, currentUser.id]);
  }

  async getAccountUsersExceptCurrent(currentUser: User) {
    const sql = `SELECT 
                    tcu.id,
                    tcu.user_id,
                    tcu.firstname,
                    tcu.lastname,
                    tcu.country_code,
                    tcu.phone,
                    tcu.email,
                    tcu.username,
                    tcu.role,
                    tcu.accountId,
                    IF((SELECT 
                            COUNT(*) AS nos
                        FROM
                            tigga_chat_connected_user_socket AS tccus
                        WHERE
                            tccus.userId = tcu.id
                                AND tccus.deleted_at IS NULL), 1, 0) AS is_online
                FROM
                    tigga_chat_user AS tcu
                WHERE 
                    tcu.accountId = ? AND tcu.id NOT IN (?) ORDER BY 11 DESC, 3 ASC`;

    return await getManager().query(sql, [
      currentUser.account.id,
      currentUser.id,
    ]);
  }


  /**
   * pick agent
   * @param userIds
   * @returns 
   */
  async pickAgent(userIds: string[]) {
    const sql = `SELECT 
                      tcu.id,
                      tcu.user_id,
                      tcu.firstname,
                      tcu.lastname,
                      tcu.email,
                      tcu.role,
                      tcu.accountId
                  FROM
                      tigga_chat_user AS tcu
                  WHERE
                      tcu.role IN ('AGENT')
                          AND tcu.id IN (?) 
                          AND (SELECT 
                              COUNT(*) AS nos
                          FROM
                              tigga_chat_connected_user_socket AS tccus
                          WHERE
                              tccus.userId = tcu.id
                                  AND tccus.deleted_at IS NULL) > 0
                  ORDER BY RAND() LIMIT 1`;

    return await getManager().query(sql, [userIds]);
  }
}
