import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayload } from '../interface/jwt-payload.interface';
import { User } from '../entity/user.entity';
import { UserRepository } from '../repository/users.repository';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private configService: ConfigService
  ) {
    super({
        secretOrKey: configService.get('JWT_SECRET_KEY'), // the scret must be same secret that was used when generating the jwt
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), // extrat jwt from request header passed as bearer token
    });
  }

  /**
   * after token is verified, nestjs call this method to validate the token based on payload..
   * here we grab user.id from payload and check if user exist in db
   * @param payload 
   * @returns 
   */
  async validate(payload: JwtPayload): Promise<User>{
      const {id} = payload;

      const user: User = await this.userRepository.findOne({id});

      if(!user){
          throw new UnauthorizedException("Invalid token!");
      }

      return user;


  }
}
