export enum Role{
    SUPER_ADMIN = 'SUPER_ADMIN',
    ADMIN = 'ADMIN',
    AGENT = 'AGENT',
    GUEST = 'GUEST',
}

export enum UserRole{
    ADMIN = 'ADMIN',
    AGENT = 'AGENT',
    GUEST = 'GUEST',
}