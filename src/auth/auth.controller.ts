import { Body, Controller, Param, Post } from '@nestjs/common';
import { AuthService } from './service/auth.service';
import { LoginUserDto } from './dto/login-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UserService } from './service/user.service';
import { Role } from './enums/role.enum';
import { CreateSpecialUserDto } from './dto/create-special-user.dto';
import { LoginSpecialUserDto } from './dto/login-speical-user.dto';

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService, private userService: UserService){

    }

    @Post('/sign-in')
    logIn(@Body() loginUserDto: LoginUserDto): Promise<{accessToken:string}>{
        return this.authService.logIn(loginUserDto);
    }

    @Post('/sign-in-special-user')
    logInSpecialUser(@Body() loginSpecialUserDto: LoginSpecialUserDto): Promise<{accessToken:string}>{
      return this.authService.logInSpecialUser(loginSpecialUserDto);
    }

    @Post('/create-special-user')
    createSuperAdmin(
      @Body() createSpecialUserDto: CreateSpecialUserDto,
    ) {
      return this.userService.createSpecialUser(createSpecialUserDto);
    }

    @Post('sign-up')
    signUp(
      @Body() createUserDto: CreateUserDto,
    ) {
      
      createUserDto.role = Role.GUEST;
      createUserDto.user_id = null;

      return this.userService.create(createUserDto);
    }
}
