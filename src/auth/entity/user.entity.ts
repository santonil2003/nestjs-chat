import { Account } from 'src/account/entity/account.entity';
import { ConnectedUserSocket } from 'src/chat/entity/connected-user-socket.entity';
import { ChatMessage } from 'src/chat/entity/chat-message.entity';
import { ChatGroup } from 'src/chat/entity/chat-group.entity';
import { SeenChatMessage } from 'src/chat/entity/seen-chat-message.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Role } from '../enums/role.enum';
import { ChatPrivateMessage } from 'src/chat/entity/chat-private-message.entity';
import { Exclude } from 'class-transformer';
import { ChatGroupsUsers } from 'src/chat/entity/chat-groups-users.entity';

@Entity({ name: 'tigga_chat_user' })
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ type: 'varchar', nullable: true, 'comment': '3rd party user identifier'})
  user_id: number;

  @Column('varchar', { length: 255 })
  firstname: string;

  @Column('varchar', { length: 255, nullable: true })
  lastname: string;

  @Column('varchar', { length: 10, nullable: true })
  country_code: string;

  @Column('varchar', { length: 20, nullable: true })
  phone: string;

  @Column('varchar', { length: 255 })
  email: string;

  @Column('varchar', { length: 255, nullable: true })
  avatar: string;

  @Column({ unique: true })
  username: string;

  @Column()
  @Exclude({ toPlainOnly: true })
  password: string;

  @Column({
    type: 'enum',
    enum: Role,
    default: Role.GUEST,
  })
  role: Role;

  @Column({ unique: true, nullable: true, comment: '3rd party token to allow direct access with the provided token'})
  access_token: string;

  @Column({type:"text", nullable: true })
  params: string;

  @Column({ nullable: true})
  last_ticket_id: string;


  @Column({ nullable: true, comment: 'Guest served by the given agent user'})
  public servedById?: string;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({ name: "servedById" })
  public servedBy?: User;



  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @Column({ nullable: true })
  accountId: string;

  /**
   * belongs to account
   */
  @ManyToOne((type) => Account, (account) => account.users, {
    eager: true,
  })
  account: Account;

  /**
   * has many connected socket
   */
  @OneToMany(
    (type) => ConnectedUserSocket,
    (connectedUserSocket) => connectedUserSocket.user,
  )
  connectedUserSockets: ConnectedUserSocket[];

  /**
   * has many chat groups or admin of may chat groups
   */
  @OneToMany((type) => ChatGroup, (chatGroup) => chatGroup.groupOwner)
  myGroups: ChatGroup[];

  /**
   * has many chat messages
   */
  @OneToMany((type) => ChatMessage, (chatMessage) => chatMessage.sentBy)
  chatMessages: ChatMessage[];

  /**
   * has many received private messages
   */
  @OneToMany(
    (type) => ChatPrivateMessage,
    (chatPrivateMessage) => chatPrivateMessage.sentTo
  )
  chatPrivateMessages: ChatPrivateMessage[];

  /**
   * belongs to one or more group
   */
  @OneToMany(
    (type) => ChatGroupsUsers,
    (chatGroupsUsers) => chatGroupsUsers.user,
    {eager: true}
  )
  chatGroups: ChatGroupsUsers[];

  /**
   * has many seem messages
   */
  @OneToMany(
    (type) => SeenChatMessage,
    (seenChatMessage) => seenChatMessage.seenBy,
  )
  seenChatMessages: SeenChatMessage[];

  /**
   * safe attributes
   * make sure you add/remove in this list once you add or remove the column.
   * @param insert
   * @returns
   */
  static getSafeAttributes(insert: boolean = false) {
    let attributes = [
      'firstname',
      'lastname',
      'country_code',
      'phone',
      'email',
      'username',
      'avatar',
      'password',
      'role',
      'access_token',
      'params',
      'last_ticket_id',
      'servedById'
    ];

    if (insert) {
      attributes.concat(['user_id', 'username']);
    } else {
      attributes.concat([]);
    }

    return attributes;
  }
}
