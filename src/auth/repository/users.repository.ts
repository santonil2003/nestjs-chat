import { ConflictException, InternalServerErrorException, Logger } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { CreateUserDto } from "../dto/create-user.dto";
import { User } from "../entity/user.entity";
import * as bcrypt from 'bcrypt';
import { Role } from "../enums/role.enum";

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    async createUser(createUserDto: CreateUserDto){

        const {
            user_id,
            firstname,
            lastname,
            country_code,
            phone,
            email,
            username,
            avatar,
            role,
            access_token,
            password
        } = createUserDto;

        try {
            
            // password hasing
            const salt = await bcrypt.genSalt();
            const hashedPassword = await bcrypt.hash(password, salt);

            const user = this.create({
                user_id,
                firstname,
                lastname,
                country_code,
                phone,
                email,
                username : username ? username : email,
                avatar,
                role: role ? role : Role.AGENT,
                access_token: access_token ? access_token : await bcrypt.hash(email, salt),
                password: hashedPassword, 

            });

            await this.save(user);

            return user;
            
        } catch (error) {

            switch(error.errno){
                case 1062:
                    throw new ConflictException('Username already exist');
                default:
                    throw new InternalServerErrorException(error.message);
            }

        }



    }
}