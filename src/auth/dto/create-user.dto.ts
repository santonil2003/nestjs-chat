import { IsEmail, IsNotEmpty, IsOptional, Matches, MaxLength, MinLength } from 'class-validator';
import {Role } from '../enums/role.enum';

export class CreateUserDto {
  @IsOptional()
  user_id: number;

  @IsNotEmpty()
  firstname: string;

  @IsNotEmpty()
  lastname: string;

  @IsOptional()
  country_code: string;

  @IsOptional()
  phone: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  username: string;

  @IsOptional()
  avatar: string;

  @IsOptional()
  role: Role;

  @IsOptional()
  access_token: string;

  @IsNotEmpty()
  password: string;

  @IsNotEmpty()
  accountId: string;

  @IsNotEmpty()
  appKey:string;

  @IsOptional()
  params: string;

  @IsOptional()
  last_ticket_id: string;

  @IsOptional()
  servedById: string;
}
