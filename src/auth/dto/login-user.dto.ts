import { IsNotEmpty, Matches, MaxLength, MinLength } from "class-validator";

export class LoginUserDto {
    @IsNotEmpty()
    username:string;

    @IsNotEmpty()
    password:string;
}