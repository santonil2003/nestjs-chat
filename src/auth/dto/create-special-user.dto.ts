import { IsEmail, IsNotEmpty, IsOptional, Matches, MaxLength, MinLength } from 'class-validator';
import {Role } from '../enums/role.enum';

export class CreateSpecialUserDto {
  @IsOptional()
  user_id: number;

  @IsNotEmpty()
  firstname: string;

  @IsNotEmpty()
  lastname: string;

  @IsOptional()
  country_code: string;

  @IsOptional()
  phone: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsOptional()
  username: string;

  @IsOptional()
  avatar: string;

  @IsOptional()
  role: Role;

  @IsOptional()
  access_token: string;

  @IsNotEmpty()
  password: string;

}
