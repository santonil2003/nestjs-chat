import { IsNotEmpty, Matches, MaxLength, MinLength } from "class-validator";

export class LoginSpecialUserDto {

    @IsNotEmpty()
    accountId:string;

    @IsNotEmpty()
    app_key:string;
    
    @IsNotEmpty()
    userId:string;
}