import { forwardRef, Module } from '@nestjs/common';
import { PassportModule} from '@nestjs/passport';
import { JwtModule} from '@nestjs/jwt';
import { AuthService } from './service/auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './repository/users.repository';
import { JwtStrategy } from './strategy/jwt.strategy';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserController } from './user.controller';
import { UserService } from './service/user.service';
import { AccountModule } from 'src/account/account.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository]),
    PassportModule.register({defaultStrategy:'jwt'}),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          secret: await configService.get('JWT_SECRET_KEY'),
          signOptions: {
            expiresIn: 36000,
          }
        }
      }
    }),
    JwtModule.register({
      secret: 'my-secret-key',
      signOptions: {
        expiresIn:3600000,
      }
    }),
    ConfigModule,
    forwardRef(() => AccountModule)
  ],
  providers: [AuthService, JwtStrategy, UserService],
  controllers: [AuthController, UserController],
  exports:[JwtStrategy, PassportModule, AuthService, UserService]
})
export class AuthModule {}
